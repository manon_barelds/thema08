*auteurs: "Manon Barelds en Jakko Geschiere"*
# README 
# __Reproductie biologisch model voor rabiës in China__ 

## Beschrijving

De file eindpodracht.Rmd is een project dat een biologisch model reproduceert. Het gaat om het [onderzoek met biologisch model](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0020891). Dit onderzoek heeft het doi nummer: 10.1371/journal.pone.0020891. Het model dat in eindopdracht.Rmd gebruikt wordt komt uit het hiervoor genoemde onderzoek.  
Het doel van het project eindopdracht.Rmd is om het model dat gebruikt is in het [onderzoek](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0020891) te implementeren en de bijbehorende figuren te reproduceren. Zo kan er gekeken worden of de reproductie van het onderzoek dezelfde resultaten geeft en of het onderzoek dus valide is.

## Installatie

Voor het project eindopdracht.Rmd is het pakket deSolve nodig. Het pakket deSolve is een add-on pakket van R. Het wordt gebruikt voor de numerieke behandeling van onder andere differentiaalvergelijkingen.  
Het pakket bevat functies die helpen bij het oplossen van differentiaalvergelijkingen (ODE), partiële differentiaalvergelijkingen (PDE), differentiële algebraïsche vergelijkingen (DAE) en vertragingsverschilvergelijkingen (DDE).  
  
Broncode en hoofddocumentatie van de nieuwste release zijn beschikbaar via het Comprehensive R Archive Network: [deSolve](https://cran.r-project.org/web/packages/deSolve/).

Gebruik de onderstaande code om het pakket deSolve te installeren .
```{r installeren} 
install.packages("deSolve")
```

Om gebruik te maken van het pakket deSolve in je project gebruik je de onderstaande code.
```{r deSolve}
library(deSolve)
```

Ook is het pakket png nodig. Het pakket png biedt een gemakkelijke en eenvoudige manier om bitmapafbeeldingen die zijn opgeslagen in het PNG-formaat te lezen, te schrijven en weer te geven. Dit pakket is in de file eindopdracht.Rmd gebruikt om een duidelijke figuur te produceren (figuur 3).

Gebruik de onderstaande code om het pakket png te installeren .
```{r installerenpng} 
install.packages("png")
```

Om gebruik te maken van het pakket png in je project gebruik je de onderstaande code.
```{r png}
library(png)
```

Het biologisch model bestaat uit 8 differentiaal vergelijkingen, deze worden met behulp van een functie geïmplementeerd in R. De onderstaande functie defineerd het model. *Voor een duidelijker beeld van de diferentiaal vergelijkingen en het bijbehorende flowdiagram zie eindopdracht.Rmd*
```{r model}
# Defineer het model 
dif_in_time <- function(t,y,parms){
  with(as.list(c(parms, y)),{
         dS <- A + (lambda * R) + (sigma * (1 - gamma) * E) - (beta * S * I) - ((m + k) * S)
         dE <- (beta * S * I) - ((m + sigma + k) * E)
         dI <- (sigma * gamma * E) - ((m + mu) * I)
         dR <- (k * (S + E)) - ((m + lambda) * R)
         
         dS1 <- B + (lambda1 * R1) + (sigma1 * (1 - gamma1) * E1) - (m1 * S1) - (beta1 * S1 * I)
         dE1 <- (beta1 * S1 * I) - ((m1 + sigma1 + k1) * E1)
         dI1 <- (sigma1 * gamma1 * E1) - ((m1 + mu1) * I1)
         dR1 <- (k1 * E1) - ((m1 + lambda1) * R1)
         
         res <- c(dS, dE, dI, dR,
                  dS1, dE1, dI1, dR1)
         return(list(res))
       }
       )
}
```

Naast het model defineren moet je ook de parameters, state (initïele waarden) en times (tijd) vaststellen. Dit doe je door de gekozen waarden te gebruiken en de onderstaande code aan te vullen.
```{r waarden}
parameters <- c(parameter1 = .. , parameter2 = .. etc.)
state <- c(state1 = .. , state2 = .. etc.)
times <- seq(begin, eind)
```
 
Nu alles gedefineerd is kun je een ode object uitvoeren. 
Het ode object is een onderdeel van het deSolve pakket. Een ode object lost een systeem op van gewone differentiaalvergelijkingen.
Een voorbeeld van een standaard ode object is hieronder beschreven. Om het te gebruiken vul je je eigen y, times, func en parms in en kies je een method.
```{r ode}
ode(y, times, func, parms, method = c("lsoda", "lsode", "lsodes", "lsodar", "vode", 
                                      "daspk", "euler", "rk4", "ode23", "ode45", "radau", 
                                      "bdf", "bdf_d", "adams", "impAdams", "impAdams_d", 
                                      "iteration"), ...)
```
Voor een ode object heb je dus nodig: y = oorspronkelijke toestand, times = tijd sequentie, func = functie, parms = parameters.
Voor meer uitleg gebruik de help functie van R (typ bijvoorbeeld 'ode' in de help).

Het aangemaakte ode object met de door jou ingevulde parameters, state, times, method en functie kun je plotten zodat je een figuur krijgt. 
```{r figuur}
plot(ode object)
```

## Benodigheden

- R studio versie 1.2.1335 (of een nieuwere versie)
- [onderzoek met het biologisch model](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0020891)
- deSolve pakket (zie installatie en gebruik)
- png pakket (zie installatie)

## parmeters en initiële waarden
Voor het project zijn de volgende parameters en intiële waarden  gebruikt.

Tabel 1 geeft de gebruikte parameters weer met de bijbehorende eenheid en uitleg.
![tabel1](figuren_hoofdartikel/tabel1_.png)

Tabel 2 geeft de gebruikte initiële waarden weer voor de toestand zonder en met infectie en uitleg wat de initiële waarden betekenen.
![tabel2](figuren_hoofdartikel/tabel2.png)

*Voor bepaalde figuren in het project eindopdracht.Rmd zijn andere waarden gebruikt dan in tabel 1 en 2 staan aangegeven. Als dit zo is dan staat dit expliciet aanagegeven bij deze figuur.*

## Ondersteuning
Als je hulp of uitleg nodig hebt of je hebt iets op te merken, stuur dan een e-mail naar;  
[m.barelds@st.hanze.nl](m.barelds@st.hanze.nl) of [j.k.geschiere@st.hanze.nl](j.k.geschiere@st.hanze.nl)