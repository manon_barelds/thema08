Schrijf een korte introductie over corticosteroiden, het gebruik ervan en het werkingsmechanisme.

In dit onderzoek wordt er gekeken naar de werking van methylprednisolon (MPL). Dit is een glucocorticosteroide. MPL heeft een invloed op de hoeveelheid repressors die aanwezig zijn. De hoeveelheid respressor wordt ook beinvloed door andere factoren. In dit onderzoek wordt er gekeken naar deze factoren zoals de synthese en afbraak van mRNA van een receptor. Dit mRNA heeft invloed op de synthese van receptoren. MPL heeft invloed op receptoren door de binding eraan. 
Wat hebben al deze factoren voor invloed op het proces en in welke mate hebben ze invloed? Wanneer is het evenwicht bereikt in de concentratie receptoren? 

In dit onderzoek wordt het glucocorticoïde receptor model dynamisch geprogrameerd. Door te kijken naar verschillende waarden van onder andere de parameters kan er gekeken worden naar de veranderingen die plaats vinden. Zo kan er gekeken worden wat de synthese en afbraak van mRNA voor invloed heeft op het aantal receptoren. Ook kan er gekeken worden naar wat voor invloed methylprednisolon (MPL) heeft op de hoeveelheid receptoren.


Doel
Het doel van dit onderzoek is om te kijken wanneer de evenwichtstoestand van het aantal receptoren is bereikt, met invloed van het glucocorticoïde medicijn methylprednisolon (MPL). Dit kan bereikt worden door het glucocorticoïde receptor dynamisch model te implementeren en te gebruiken. Verder wordt er een figuur geproduceerd met het verloop van het aantal receptoren per tijdseenheid.

De verwachting is dat als de concentratie van MPL veranderd dit invloed heeft op het aantal receptoren. De verwachting is dat de synthese van het receptor mRNA en de fractie vrije receptoren (de gebonden receptoren die weer losse receptoren worden dus de gerecyclede receptoren) factoren het meeste invloed hebben op de hoeveelheid receptoren en dus op het evenwicht hiervan.

Theorie:

**Corticoïden** \newline
Corticoïden zijn steroïde-hormonen die door de bijnierschors worden geproduceerd.\newline

**Glucocorticoïden** \newline
De glucocorticoïden vormen een groep van de corticoïden. De glucocorticoïden danken hun naam aan het feit dat het dus corticosteroïden zijn met een effect op de regulatie van het glucosemetabolisme. De naam is uit elkaar te halen als 'glucose cortex steroïde. \newline
Glucocorticosteroiden spelen een belangrijke rol in beinvloeding van de mate van transcriptie van ontstekingsfactoren. Daarnaast hebben ze een remmend effect op de transcriptie van de eigen receptoren door binding van het geactiveerde steroide-receptor complex aan het glucocorticoid respons element.
Hierom worden glucocorticosteroïden in de geneeskunde gebruikt voor de behandeling van ontstekingsreacties, wanneer het eigen afweersysteem tekort schiet. Ook kunnen ze gebruikt worden om allergische reacties te verminderen, zoals bij asthma.\newline

**Methylprednisolon (MPL)** \newline
Methylprednisolon is een corticosteroïdmedicijn dat wordt gebruikt om het immuunsysteem te onderdrukken en ontstekingen te verminderen. Aandoeningen waarbij het wordt gebruikt zijn onder andere huidziekten, reumatische aandoeningen, allergieën, astma, COPD, bepaalde kankers en als aanvullende therapie voor tuberculose. MPL wordt via de mond toegediend, via injectie in een ader of spier maar het kan ook op de huid worden aangebracht.
Een paar ernstige bijwerkingen zijn psychische problemen krijgen en een verhoogd risico op infecties krijgen. \newline  

**Receptoren** \newline
Receptoren zijn eiwitten in het celmembraan, het cytoplasma of de celkern, waaraan een specifiek molecuul kan binden. Receptoren kunnen signalen van binnen of buiten de cel doorgeven. Wanneer een signaalmolecuul aan een receptor bindt, kan de receptor een cellulaire respons op gang brengen. 

Glucocorticoïden zoals Methylprednisolon (MPL) kunnen door het celmembraan heen en ze kunnen met een hoge affiniteit binden aan specifieke (cytoplasmatishe) receptoren. Door de binding aan deze receptoren kan de transcriptie en de eiwit synthese gemodificeerd worden. Hierdoor kan MPL bijvoorbeeld helpen bij de infiltratie van ontstekings veroorzakers. Ook kan MPL helpen bij het onderdrukken van de humorale immuunreacties.

Het biologische model houdt in dat er een synthese en een afbraak van mRNA is dat codeerd voor repressors. De synthese van het mRNA wordt onderdrukt door een repressie factor genaamd IC50_Rm. IC50_Rm is de concentratie van MPL-receptor complex waarbij de aanmaak van receptor mRNA daalt tot 50% van de basis waarde. IC50_Rm is in het model geintroduceerd omdat er gecorsrigeerd moet worden voor deze onderdrukking. \newline
Het aantal receptoren hangt af van verschillende factoren binnen het model. Sommige receptoren binden aan MPL maar niet allemaal. Een gedeelte van de gebonden receptoren (MPL-receptor complex) transporteren zich naar de celkern, deze worden hier verder gebruikt. Een ander deel van de de gebonden receptoren wordt gerecycled tot weer een losse receptor. 
Doordat er verschillende wegen zijn die de receptor kan volgen komen er verchillende dingen kijken bij het biologische model, er moet gecorrigeerd worden voor bepaalde dingen. 


